Esta es la solución a un ejercicio propuesto en clase.
Un requisito era no usar funciones de la librería estándar de C o C++ con el
objetivo de practicar con arrays, punteros, literales de cadenas y la gestión
dinámica de memoria con new y delete.
El código ha superado diversas pruebas pero no se ha verificado a fondo, por
lo que podría haber algún bug no detectado.
Para comentar cualquier cosa acerca del código escribir un correo electrónico
a angel.rodriguez@esne.edu.
