
// Coded by Ángel in 2020
// This source code is released into the public domain.
// angel.rodriguez@esne.edu

#ifndef EXAMPLE_UTILS
#define EXAMPLE_UTILS

    namespace utils
    {

        /** Devuelve la longitud en caracteres de la cadena recibida como parámetro sin tener en
          * cuenta el carácter terminador (\0), que solamente se usa para determinar dónde termina.
          * @param input La cadena cuya longitud se medirá.
          * @return La longitud en caracteres de la cadena recibida.
          */
        size_t length_of (const char * input);

        /** Devuelve el número de palabras que hay dentro de una cadena separadas por cierto
          * carácter.
          * @param input La cadena cuyas palabras se contarán.
          * @param separator Carácter que se usará para identificar la separación entre palabras.
          * @return El número de palabras que se encontró.
          */
        size_t count_words_in (const char * input, const char separator = ' ');

        /** Compara alfabéticamente dos cadenas hasta que se encuentre cierto carácter terminador
          * en alguna de ellas.
          * @param a La primera cadena que se comparará.
          * @param b La segunda cadena que se comparará.
          * @param separator Carácter que se usará para identificar la separación entre palabras.
          * @returns -1 si la primera cadena precede a la segunda alfabéticamente.<br>
          *     +1 si la segunda cadena precede a la primera alfabéticamente.<br>
          *     0 si ambas cadenas son iguales.<br>
          *     Si una cadena es prefijo de otra más larga, se considera que la más corta precede
          *     a la que es más larga.
          */
        int compare (const char * a, const char * b, const char terminator = 0);

        /** Ordena alfabéticamente las palabras que se encuentran dentro de la cadena recibida
          * y devuelve el resultado en una nueva cadena.
          * @param input La cadena cuyas palabras se ordenarán.
          * @param separator Carácter que se usará para identificar la separación entre palabras.
          * @return Devuelve una nueva cadena que tiene las palabras ordenadas alfabéticamente.
          *     <b>Dicha cadena se debe descartar usando el operador delete[].</b>
          *     Puede retornar nullptr si la entrada es un puntero nulo o si no se ha podido
          *     alojar memoria suficiente.
          */
        char * sort_words (const char * input, const char separator = ' ');

    }

#endif
