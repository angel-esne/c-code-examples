
// Coded by Ángel in 2020
// This source code is released into the public domain.
// angel.rodriguez@esne.edu

#include <new>
#include <cassert>
#include <cstddef>

using namespace std;

namespace utils
{

    size_t length_of (const char * input)
    {
        size_t length = 0;

        if (input) while (input[length] != 0) ++length;             // Incrementar length mientras no
                                                                    // se encuentre un carácter nulo
        return length;
    }

    // ------------------------------------------------------------------------------------------ //

    size_t count_words_in (const char * input, const char separator = ' ')
    {
        size_t count = 0;

        if (input)
        {
            for (bool word = false; *input; ++input)
            {
                if (word)                                           // Se está recorriendo una palabra
                {
                    if (*input == separator) word = false;
                }
                else                                                // Se está recorriendo uno o varios
                {                                                   // separadores
                    if (*input != separator) word = true, ++count;
                }
            }
        }

        return count;
    }

    // ------------------------------------------------------------------------------------------ //

    int compare (const char * a, const char * b, const char terminator = 0)
    {
        if (!a) return -1;                                          // Si es nula, la cadena se considera
        if (!b) return +1;                                          // vacía y prefijo por ser más corta

        while (*a != terminator && *b != terminator)                // Mientras no se llegue al final
        {                                                           // del alguna de las dos cadenas
            if (*a < *b) return -1;
            if (*a > *b) return +1;

            ++a; ++b;
        }

        if (*a == terminator && *b == terminator) return 0;         // Son iguales si se llegó al final de ambas
        if (*a != terminator) return +1;                            // Se valora si una cadena es prefijo
        if (*b != terminator) return -1;                            // de otra

        return 0;                                                   // Nunca se debería llegar aquí, pero el compilador
    }                                                               // se puede quejar si no encuentra un return

    // ------------------------------------------------------------------------------------------ //

    char * sort_words (const char * input, const char separator = ' ')
    {
        char * output = nullptr;

        if (input)
        {
            // Se cuentan las letras y las palabras que hay en la cadena de entrada:

            size_t total_length = length_of (input);
            size_t total_words  = count_words_in (input);

            // Se reserva memoria para la cadena de salida, que nunca será más larga que la de entrada:

            output = new (nothrow) char[total_length + 1];      // std::notrow evita que new lance excepciones
                                                                // en caso de ocurrir algún problema
            if (output)                                         // Si ocurre un problema output guardará nullptr
            {
                char * out = output;                            // El puntero out se usará para copiar palabras
                                                                // en la cadena de salida
                if (total_length > 0)
                {
                    // El puntero in se usará para leer palabras de la cadena de entrada y el puntero words
                    // apunta al inicio de un array de punteros, cada uno de los cuales apunta, a su vez, al
                    // inicio de cada palabra:

                    const char  * in    = input;
                    const char ** words = new (nothrow) const char * [total_words];

                    if (words)                                  // Se comprueba si hubo problemas al alojar memoria
                    {
                        // Este bucle busca y guarda el inicio de cada palabra en el array de punteros
                        // al que apunta words:

                        size_t index = 0;

                        for (bool word = false; *in; ++in)
                        {
                            if (word)
                            {
                                if (*in == separator) word = false;
                            }
                            else
                            {
                                if (*in != separator) word = true, words[index++] = in;
                            }
                        }

                        // Dentro del siguiente bucle se busca la primera palabra alfabéticamente, se copia
                        // a la salida, se descarta y se repite el proceso hasta que se hayan copiado todas
                        // las palabras:

                        size_t remaining_words = total_words;

                        do
                        {
                            // Se busca en el array al que apunta words el primer puntero que no sea nulo
                            // (un puntero nulo indica que la palabra se descartó) para encontrar el inicio
                            // de la primera palabra a tener en cuenta:

                            size_t first = 0;

                            while (first < total_words)
                            {
                                if (words[first]) break; else ++first;
                            }

                            // En este punto no debería ocurrir que first >= total_words:

                            assert(first < total_words);

                            // Teniendo ya una palabra por la que empezar, se busca la palabra que
                            // se deba colocar primero alfabéticamente:

                            size_t next = first + 1;

                            while (next < total_words)
                            {
                                if (words[next] && compare (words[first], words[next], separator) > 0)
                                {
                                    first = next;
                                }

                                ++next;
                            }

                            // En este punto first guarda el índice de la palabra que se debe colocar
                            // primero alfabéticamente de entre las que quedan.

                            // Si no es la primera palabra que se guarda en la salida, se añade un
                            // espacio delante para separarla de las anteriores:

                            if (remaining_words < total_words) *out++ = separator;

                            // Se copia la palabra añadiéndola al final de la salida:

                            for (in = words[first]; *in && *in != separator; ) *out++ = *in++;

                            // Se descarta el puntero a la palabra copiada para que no se vuelva a
                            // tener en cuenta:

                            words[first] = nullptr;
                        }
                        while (--remaining_words > 0);

                        // Se libera la memoria del array de punteros a palabras:

                        delete [] words;
                    }
                }

                // Se añade un carácter nulo al final de la cadena de salida para marcar dónde termina:

                *out = 0;
            }
        }

        return output;
    }

}
