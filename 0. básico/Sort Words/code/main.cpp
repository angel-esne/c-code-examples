
// Coded by Ángel in 2020
// This source code is released into the public domain.
// angel.rodriguez@esne.edu

#include <iostream>
#include "utils.hpp"

using namespace std;
using namespace utils;

int main ()
{
    const char input[] =
        "lorem ipsum dolor sit amet, consectetur adipiscing elit, sed "
        "do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

    char * output = sort_words (input);

    if (output)
    {
        cout << "INPUT:  \n\"" <<  input << '"' << '\n' << endl;
        cout << "OUTPUT: \n\"" << output << '"' << endl;

        // Según la documentación de la función sort_words(), el resultado debe ser destruido
        // usando el operador delete cuando ya no se necesite:

        delete [] output;
    }
    else
    {
        cout << "Failed to sort the words." << endl;
    }

    return 0;
}
