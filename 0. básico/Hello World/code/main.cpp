
// Autor: Ángel
// Fecha: Oct/2019
// Este código es de dominio público.
// angel.rodriguez@esne.edu

#include <iostream>                 // Se incluye la cabecera de la biblioteca estándar en la que está declarado std::cout

int main ()                         // Se define la función main(), que es donde se inicia todo programa escrito en C++
{
    std::cout << "Hello, World";    // Se escribe un mensaje en la salida estándar, que normalmente es la consola

    return 0;                       // El programa termina cuando se sale de main(). Se retorna un 0 al sistema operativo.
}
