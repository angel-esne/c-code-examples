
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2020.10

#include <gtest/gtest.h>
#include <Memory_Pool.hpp>
#include <Object_Pool.hpp>

using namespace utils;

TEST(MemoryPoolTests, allocation)
{
    Memory_Pool<4> memory_pool(1);

    auto chunk = memory_pool.allocate ();

    ASSERT_TRUE(chunk != nullptr);

    auto empty = memory_pool.allocate ();

    ASSERT_TRUE(empty == nullptr);
}

TEST(ObjectPoolTests, allocation)
{
    Object_Pool< int > object_pool(1);

    auto item = object_pool.allocate ();

    ASSERT_TRUE(item != nullptr);

    auto null = object_pool.allocate ();

    ASSERT_TRUE(null == nullptr);

    object_pool.free (item);
}

TEST(ObjectPoolTests, allocation_argument_forwarding)
{
    static bool  empty_constructor_was_called = false;
    static bool   copy_constructor_was_called = false;
    static bool   move_constructor_was_called = false;
    static bool  value_constructor_was_called = false;
    static bool values_constructor_was_called = false;

    struct Type
    {
        Type(              ) {  empty_constructor_was_called = true; }
        Type(      Type && ) {   move_constructor_was_called = true; }
        Type(const Type &  ) {   copy_constructor_was_called = true; }
        Type(const char *  ) {  value_constructor_was_called = true; }
        Type(int , double  ) { values_constructor_was_called = true; }
    };

    Object_Pool< Type > object_pool(5);

    auto a = object_pool.allocate ();

    ASSERT_TRUE(empty_constructor_was_called);

    auto b = object_pool.allocate (std::move (*a));

    ASSERT_TRUE(move_constructor_was_called);

    auto c = object_pool.allocate (*a);

    ASSERT_TRUE(copy_constructor_was_called);

    auto d = object_pool.allocate ("test");

    ASSERT_TRUE(value_constructor_was_called);

    auto e = object_pool.allocate (0, 0.0);

    ASSERT_TRUE(values_constructor_was_called);

    object_pool.free (a);
    object_pool.free (b);
    object_pool.free (c);
    object_pool.free (d);
    object_pool.free (e);
}

int main (int argument_count, char * arguments[])
{
    testing::InitGoogleTest(&argument_count, arguments);

    return RUN_ALL_TESTS ();
}
