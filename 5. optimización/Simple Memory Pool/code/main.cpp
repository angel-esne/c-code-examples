
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.10

#include <iostream>
#include <vector>
#include "Memory_Pool.hpp"

using namespace std;

class Test
{
public:

    Test() { cout << "Test constructor\n"; }
   ~Test() { cout << "Test destructor \n"; }

};

int main ()
{
    constexpr size_t items = 10;
    
    vector< Test * > pool_objects(items);

    utils::Object_Pool< Test > object_pool(items);

    for (size_t count = 0; count < items; ++count)
    {
        pool_objects[count] = object_pool.allocate ();
    }

    for (size_t count = 0; count < items; ++count)
    {
        object_pool.free (pool_objects[count]);
    }

    cout << endl;

    return 0;
}
