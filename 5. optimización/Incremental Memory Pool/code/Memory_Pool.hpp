
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.10

#pragma once

#include <new>
#include <cassert>
#include <cstddef>

using std::byte;                    // Requiere C++17

namespace utils
{

    class Memory_Pool
    {

        byte * pool_start;          ///< Puntero al primer byte del array de bytes de la memory pool
        byte * pool_end;            ///< Puntero al byte destrás del último byte de la memory pool
        byte * pool_trail;          ///< Puntero al byte destrás del último byte reservado
        size_t pool_size;           ///< Tamaño en bytes de la memory pool

    public:

        /** @param desired_pool_size Tamaño en bytes de la memory pool.
          */
        Memory_Pool(size_t desired_pool_size)
        {
            pool_start = new (std::nothrow) byte[desired_pool_size];
            pool_trail = pool_start;
            pool_size  = pool_start ? desired_pool_size : 0;
            pool_end   = pool_start + pool_size;
        }

       ~Memory_Pool()
        {
            delete [] pool_start;
        }

        /** @return Retorna true si la memory pool se construyó sin problemas.
          */ 
        bool is_ok () const
        {
            return pool_size > 0;
        }

        /** @return Retorna el tamaño en bytes que se reservó para la memory pool. 0 si hubo algún error.
          */ 
        size_t size () const
        {
            return pool_size;
        }

        /** @return Retorna la cantidad de memoria que se ha reservado hasta el momento.
          */ 
        size_t consumed () const
        {
            return size_t(pool_trail - pool_start);
        }

        /** @return Retorna la cantidad de memoria en bytes disponible.
          */ 
        size_t available () const
        {
            return pool_size - consumed ();
        }

        /** Este método muestra cómo se puede reservar memoria de la memory pool de un modo trivial.
          * No está teniendo en cuenta la alineación de cada bloque reservado ni otros detalles.
          * Esta versión sin alineación solo debería usarse si el tamaño reservado permite una
          * alineación automática (8, 16, etc.) en cuyo caso se consigue una pequeña ventaja.
          * @param  amount Cantidad de bytes que se desea reservar. Con el valor cero se retorna una
          *     dirección de memoria válida.
          * @return Retorna un puntero al inicio del bloque reservado o nullptr si no hay suficiente
          *     memoria disponible.
          */
        void * allocate (size_t amount)
        {
            return (pool_trail += amount) > pool_end ? (pool_trail -= amount, nullptr) : pool_trail;
        }

        /** Este método muestra cómo se puede reservar memoria de la memory pool manteniendo la
          * alineación requerida.
          * La alineación es un parámetro de plantilla porque se requiere un valor conocido en tiempo
          * de compilación. Queda cierto margen para la optimización a costa de la claridad.
          * @param  amount Cantidad de bytes que se desea reservar. Con el valor cero se retorna una
          *     dirección de memoria válida.
          * @return Retorna un puntero al inicio del bloque reservado o nullptr si no hay suficiente
          *     memoria disponible.
          */
        template< size_t  ALIGNMENT >
        void * allocate (size_t amount)
        {
            static_assert(ALIGNMENT > 0);

            ptrdiff_t remainder = (pool_trail - pool_start) % ALIGNMENT;
            size_t    padding   =  remainder  ? ALIGNMENT - remainder : 0;

            return (pool_trail +=  padding + amount) > pool_end
                ?  (pool_trail -=  padding + amount, nullptr)
                :   pool_trail;
        }

    };

}
