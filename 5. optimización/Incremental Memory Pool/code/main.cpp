
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.10
//
// Este ejemplo realiza un benchmark sintético entre usar una memory pool incremental o usar el
// operador new. Para que la comparación sea válida se debe compilar y ejecutar el programa en
// configuración Release. También es recomendable usar tipos POD (se puede configurar cambiando
// TYPE más abajo), ya que es el uso típico de una memory pool incremental.
// Es habitual que la memory pool sea entre 20 y 40 veces más rápida que el operador new, aunque
// el resultado depende de la versión del sistema operativo, del modelo de procesador y del tipo
// de dato usado, entre otros factores.

#include <chrono>
#include <iomanip>
#include <iostream>
#include <vector>
#include "Memory_Pool.hpp"

using namespace std;
using namespace std::chrono;

struct  Point4 { float x, y, z, w; };           // Tipos POD para hacer pruebas
struct  Odd    { char  c[3];       };

typedef Point4 TYPE;                            // Tipo usado para la instanciación

int main ()
{
    constexpr size_t iterations = 1000000;

    high_resolution_clock::time_point start, finish;

    cout << "Running heap allocation...";

    start = high_resolution_clock::now ();
    {
        vector< TYPE * > heap_objects(iterations);

        for (size_t count = 0; count < iterations; ++count)
        {
            heap_objects[count] = new TYPE;
        }

        for (size_t count = 0; count < iterations; ++count)
        {
            delete heap_objects[count];
        }
    }
    finish = high_resolution_clock::now ();

    double heap_elapsed = duration_cast< duration< double > >(finish - start).count ();

    cout << "\nHeap allocation required " << heap_elapsed << " seconds.";
    cout << "\nRunning pool allocation...";

    start  = high_resolution_clock::now ();
    {
        vector< TYPE * > pool_objects(iterations);

        utils::Memory_Pool memory_pool(sizeof(TYPE));

        for (size_t count = 0; count < iterations; ++count)
        {
            pool_objects[count] = new (memory_pool.allocate (sizeof(TYPE))) TYPE;
        }

        for (size_t count = 0; count < iterations; ++count)
        {
            pool_objects[count]->~TYPE ();
        }
    }
    finish = high_resolution_clock::now ();

    double pool_elapsed = duration_cast< duration< double > >(finish - start).count ();

    cout << "\nPool allocation required " << pool_elapsed << " seconds.";
    cout << "\nPool allocator performance vs heap allocator: ";
    cout << setprecision (2) << (heap_elapsed / pool_elapsed) << " times.";
    cout << endl;

    return 0;
}
