/*
 * Copyright � 2020+ �ngel Rodr�guez Ballesteros
 * 
 * Distributed under the Boost Software License, version 1.0
 * See the LICENSE file or www.boost.org/LICENSE_1_0.txt
 * 
 * angel.rodriguez@esne.edu
 * 2020.11
 */

#pragma once

#include "Cartesian.hpp"
#include "enable_if.hpp"
#include <algorithm>
#include <utility>

namespace math
{

    namespace internal
    {

        template
        <
            unsigned DIMENSION,
            typename VALUE_TYPE = float,
            template< unsigned >  class COORDINATE_SYSTEM = Cartesian
        >
        class Coordinates_Base : public COORDINATE_SYSTEM< DIMENSION >
        {
        public:

            using  Value_Type        = VALUE_TYPE;
            using  Coordinate_System = COORDINATE_SYSTEM< DIMENSION >;

            static constexpr unsigned dimension = Coordinate_System::dimension;
            static constexpr unsigned size      = Coordinate_System::size;

        protected:

            Value_Type values[size];

        public:

            Coordinates_Base() = default;
            Coordinates_Base(const Coordinates_Base & ) = default;

            Coordinates_Base(const Value_Type (& array)[size])
            {
                std::copy_n (array, size, this->values);
            }

        public:

            Coordinates_Base & operator = (const Coordinates_Base & ) = default;

            Value_Type & operator [] (unsigned index)
            {
                return values[index];
            }

            const Value_Type & operator [] (unsigned index) const
            {
                return values[index];
            }

            bool operator == (const Coordinates_Base & other) const
            {
                return std::equal (this->values, this->values + size, other.values);
            }

            bool operator != (const Coordinates_Base & other) const
            {
                return not (*this == other);
            }

            explicit operator Value_Type * ()
            {
                return values;
            }

            explicit operator const Value_Type * () const
            {
                return values;
            }

        };

    }

    template
    <
        unsigned DIMENSION,
        typename VALUE_TYPE = float,
        template< unsigned >  class COORDINATE_SYSTEM = Cartesian
    >
    class Coordinates : public internal::Coordinates_Base< DIMENSION, VALUE_TYPE, COORDINATE_SYSTEM >
    {
                
        using Coordinates_Base = internal::Coordinates_Base< DIMENSION, VALUE_TYPE, COORDINATE_SYSTEM >;

    public:

        Coordinates() = default;
        Coordinates(const Coordinates & ) = default;

        template< typename ... ARGUMENTS >
        explicit Coordinates(const ARGUMENTS & ... arguments) : Coordinates_Base(arguments...)
        {
        }

    };

    template< typename VALUE_TYPE >
    class Coordinates< 1, VALUE_TYPE, Cartesian > : public internal::Coordinates_Base< 1, VALUE_TYPE, Cartesian >
    {
                
        using Coordinates_Base = internal::Coordinates_Base< 1, VALUE_TYPE, Cartesian >;

    protected:

        using Coordinates_Base::values;

    public:

        using typename Coordinates_Base::Value_Type;

    public:

        Coordinates() = default;
        Coordinates(const Coordinates & ) = default;

        Coordinates(const Value_Type & x)
        {
            values[0] = x;
        }

        template< typename ... ARGUMENTS >
        explicit Coordinates(const ARGUMENTS & ... arguments) : Coordinates_Base(arguments...)
        {
        }

    public:

              Value_Type & x ()       { return values[0]; }
        const Value_Type & x () const { return values[0]; }

    };

    template< typename VALUE_TYPE >
    class Coordinates< 2, VALUE_TYPE, Cartesian > : public internal::Coordinates_Base< 2, VALUE_TYPE, Cartesian >
    {
                
        using Coordinates_Base = internal::Coordinates_Base< 2, VALUE_TYPE, Cartesian >;

    protected:

        using Coordinates_Base::values;

    public:

        using typename Coordinates_Base::Value_Type;

    public:

        Coordinates() = default;
        Coordinates(const Coordinates & ) = default;

        Coordinates(const Value_Type & x, const Value_Type & y)
        {
            values[0] = x;
            values[1] = y;
        }

        template< typename ... ARGUMENTS >
        explicit Coordinates(const ARGUMENTS & ... arguments) : Coordinates_Base(arguments...)
        {
        }

    public:

              Value_Type & x ()       { return values[0]; }
        const Value_Type & x () const { return values[0]; }
              Value_Type & y ()       { return values[1]; }
        const Value_Type & y () const { return values[1]; }

    };

    template< typename VALUE_TYPE >
    class Coordinates< 3, VALUE_TYPE, Cartesian > : public internal::Coordinates_Base< 3, VALUE_TYPE, Cartesian >
    {
                
        using Coordinates_Base = internal::Coordinates_Base< 3, VALUE_TYPE, Cartesian >;

    protected:

        using Coordinates_Base::values;

    public:

        using typename Coordinates_Base::Value_Type;

    public:

        Coordinates() = default;
        Coordinates(const Coordinates & ) = default;

        Coordinates(const Value_Type & x, const Value_Type & y, const Value_Type & z)
        {
            values[0] = x;
            values[1] = y;
            values[2] = z;
        }

        template< typename ... ARGUMENTS >
        explicit Coordinates(const ARGUMENTS & ... arguments) : Coordinates_Base(arguments...)
        {
        }

    public:

              Value_Type & x ()       { return values[0]; }
        const Value_Type & x () const { return values[0]; }
              Value_Type & y ()       { return values[1]; }
        const Value_Type & y () const { return values[1]; }
              Value_Type & z ()       { return values[2]; }
        const Value_Type & z () const { return values[2]; }

    };

    template< typename VALUE_TYPE >
    class Coordinates< 4, VALUE_TYPE, Cartesian > : public internal::Coordinates_Base< 4, VALUE_TYPE, Cartesian >
    {
                
        using Coordinates_Base = internal::Coordinates_Base< 4, VALUE_TYPE, Cartesian >;

    protected:

        using Coordinates_Base::values;

    public:

        using typename Coordinates_Base::Value_Type;

    public:

        Coordinates() = default;
        Coordinates(const Coordinates & ) = default;

        Coordinates(const Value_Type & x, const Value_Type & y, const Value_Type & z, const Value_Type & t)
        {
            values[0] = x;
            values[1] = y;
            values[2] = z;
            values[3] = t;
        }

        template< typename ... ARGUMENTS >
        explicit Coordinates(const ARGUMENTS & ... arguments) : Coordinates_Base(arguments...)
        {
        }

    public:

              Value_Type & x ()       { return values[0]; }
        const Value_Type & x () const { return values[0]; }
              Value_Type & y ()       { return values[1]; }
        const Value_Type & y () const { return values[1]; }
              Value_Type & z ()       { return values[2]; }
        const Value_Type & z () const { return values[2]; }
              Value_Type & t ()       { return values[3]; }
        const Value_Type & t () const { return values[3]; }

    };

}
