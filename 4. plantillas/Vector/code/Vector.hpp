/*
 * Copyright © 2020+ Ángel Rodríguez Ballesteros
 * 
 * Distributed under the Boost Software License, version 1.0
 * See the LICENSE file or www.boost.org/LICENSE_1_0.txt
 * 
 * angel.rodriguez@esne.edu
 * 2020.11
 */

#pragma once

#include <cmath>
#include <ostream>
#include "Coordinates.hpp"

namespace math
{

    //////////////////////////////////////////////// VECTOR /////////////////////////////////////////////////

    template
    <
        unsigned DIMENSION,
        typename VALUE_TYPE = float,
        template< unsigned >  class COORDINATE_SYSTEM = Cartesian
    >
    class Vector;

    ////////////////////////////////////////// VECTOR / CARTESIAN ///////////////////////////////////////////

    template< unsigned DIMENSION, typename VALUE_TYPE >
    class Vector< DIMENSION, VALUE_TYPE, Cartesian > : public Coordinates< DIMENSION, VALUE_TYPE, Cartesian >
    {
    public:

        using Coordinates = Coordinates< DIMENSION, VALUE_TYPE, Cartesian >;

        using typename Coordinates::Coordinate_System;
        using typename Coordinates::Value_Type;

        using Coordinates::dimension;
        using Coordinates::size;

    protected:

        using Coordinates::values;

    public:

        Vector() = default;
        Vector(const Vector & ) = default;

        template< typename ... ARGUMENTS >
        explicit Vector(const ARGUMENTS & ... arguments) : Coordinates(arguments...)
        {
        }

    public:

        Value_Type squared_modulus () const
        {
            Value_Type result = Value_Type(0);

            for (unsigned i = 0; i < dimension; ++i) result += values[i] * values[i];

            return result;
        }

        template< typename NUMERIC_TYPE >
        NUMERIC_TYPE modulus () const
        {
            return SQRT (NUMERIC_TYPE(squared_modulus ()));
        }

        template< typename NUMERIC_TYPE >
        NUMERIC_TYPE length () const
        {
            return modulus< NUMERIC_TYPE > ();
        }

    };

    // Definiciones abreviadas de tipos de uso habitual:

    template< template< unsigned > class COORDINATE_SYSTEM > using Vector1i_ = Vector< 1,    int, COORDINATE_SYSTEM >;
    template< template< unsigned > class COORDINATE_SYSTEM > using Vector1f_ = Vector< 1,  float, COORDINATE_SYSTEM >;
    template< template< unsigned > class COORDINATE_SYSTEM > using Vector1d_ = Vector< 1, double, COORDINATE_SYSTEM >;

    using Vector1i = Vector< 1, int   , Cartesian >;
    using Vector1f = Vector< 1, float , Cartesian >;
    using Vector1d = Vector< 1, double, Cartesian >;

    using Vector2i = Vector< 2, int   , Cartesian >;
    using Vector2f = Vector< 2, float , Cartesian >;
    using Vector2d = Vector< 2, double, Cartesian >;

    using Vector3i = Vector< 3, int   , Cartesian >;
    using Vector3f = Vector< 3, float , Cartesian >;
    using Vector3d = Vector< 3, double, Cartesian >;

    using Vector4i = Vector< 4, int   , Cartesian >;
    using Vector4f = Vector< 4, float , Cartesian >;
    using Vector4d = Vector< 4, double, Cartesian >;

}
