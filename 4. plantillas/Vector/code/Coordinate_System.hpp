/*
 * Copyright � 2020+ �ngel Rodr�guez Ballesteros
 * 
 * Distributed under the Boost Software License, version 1.0
 * See the LICENSE file or www.boost.org/LICENSE_1_0.txt
 * 
 * angel.rodriguez@esne.edu
 * 2020.11
 */

#pragma once

namespace math
{

    template< unsigned DIMENSION >
    class Coordinate_System
    {
    public:

        static constexpr unsigned dimension = DIMENSION;
        static constexpr unsigned size      = DIMENSION;

    };

}
