/*
 * Copyright � 2020+ �ngel Rodr�guez Ballesteros
 * 
 * Distributed under the Boost Software License, version 1.0
 * See the LICENSE file or www.boost.org/LICENSE_1_0.txt
 * 
 * angel.rodriguez@esne.edu
 * 2020.11
 */

#pragma once

#include "Coordinate_System.hpp"

namespace math
{

    template< unsigned DIMENSION >
    class Cartesian : public Coordinate_System< DIMENSION >
    {
    };

    template< > class Cartesian< 1 > : public Coordinate_System< 1 > { public: enum { X }; };
    template< > class Cartesian< 2 > : public Coordinate_System< 2 > { public: enum { X, Y }; };
    template< > class Cartesian< 3 > : public Coordinate_System< 3 > { public: enum { X, Y, Z }; };
    template< > class Cartesian< 4 > : public Coordinate_System< 4 > { public: enum { X, Y, Z, T }; };

}
