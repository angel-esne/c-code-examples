
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.10

#include <cassert>
#include <iostream>
#include <type_traits>
#include "Vector.hpp"
#include "Coordinates.hpp"

using namespace std;
using namespace math;

class X
{
public:

    X(int i, int j) { }

    X(std::initializer_list<int> i) { }

};


int main ()
{
    X x{ 0, 1 };

    static_assert(is_pod< Vector3f >::value, "PROBLEM: Vector<T> is not POD.");

    Coordinates< 2 > c1(0.f, 0.f);
    Coordinates< 2 > c2({ 1.f, 2.f });
    Coordinates< 2 > c3(c2);

    assert(c2[0] == c3[0] && c2[1] == c3[1]);

    float array3f[] = { 1.f, 2.f, 3.f };

    Vector3f v0;
    Vector3f v1(0.f, 0.f, 0.f);
    Vector3f v2(array3f);
    Vector3f v3(v2);

    assert(v2.x() == v3.x() && v2.y() == v3.y() && v2.z() == v3.z());

    Vector1i v1d1(1);
    Vector2i v2d1(1, 2);
    Vector3i v3d1(1, 2, 3);
    Vector4i v4d1(1, 2, 3, 4);
    Vector1i v1d2{1};
    Vector2i v2d2{1, 2};
    Vector3i v3d2(1, 2, 3);
    Vector4i v4d2{1, 2, 3, 4};

    return 0;
}
