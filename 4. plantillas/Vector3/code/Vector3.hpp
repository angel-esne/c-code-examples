
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.11

#pragma once

#include <cmath>
#include <ostream>
#include <type_traits>

namespace math
{

    template< typename TYPE >
    class Vector3
    {
    public:

        static constexpr bool is_integer = std::is_integral      < TYPE >::value;
        static constexpr bool is_real    = std::is_floating_point< TYPE >::value;

    private:

        TYPE coordinates[3];

    public:

        Vector3() = default;

        Vector3(TYPE x, TYPE y, TYPE z)
        {
            coordinates[0] = x;
            coordinates[1] = y;
            coordinates[2] = z;
        }

        Vector3(TYPE (& array)[3])
        {
            coordinates[0] = array[0];
            coordinates[1] = array[1];
            coordinates[2] = array[2];
        }

    public:

        TYPE & x ()       { return coordinates[0]; }
        TYPE   x () const { return coordinates[0]; }
        TYPE & y ()       { return coordinates[1]; }
        TYPE   y () const { return coordinates[1]; }
        TYPE & z ()       { return coordinates[2]; }
        TYPE   z () const { return coordinates[2]; }

    public:

        TYPE squared_modulus () const
        {
            return x () * x () + y () * y () + z () * z ();
        }

        TYPE modulus () const
        {
            static_assert(Vector3::is_real, "math::Vector3<T>::modulus() requires a floating point base TYPE.");
            return sqrt (squared_modulus ());
        }

        TYPE length () const
        {
            return modulus ();
        }

        Vector3 & normalize ()
        {
            return *this /= modulus ();
        }

        Vector3 normalized () const
        {
            return *this /  modulus ();
        }

        TYPE dot (const Vector3 & other) const
        {
            return this->x () * other.x () + this->y () * other.y () + this->z () * other.z ();
        }

        Vector3 cross (const Vector3 & other) const
        {
            return
            {
                this->y () * other.z () - other.y () * this->z (),
                this->z () * other.x () - other.z () * this->x (),
                this->x () * other.y () - other.x () * this->y ()
            };
        }

    public:

        Vector3 & operator += (const Vector3 & other)
        {
            this->x () += other.x ();
            this->y () += other.y ();
            this->z () += other.z ();
            return *this;
        }

        Vector3 & operator -= (const Vector3 & other)
        {
            this->x () -= other.x ();
            this->y () -= other.y ();
            this->z () -= other.z ();
            return *this;
        }

        Vector3 & operator *= (const TYPE & value)
        {
            x () *= value;
            y () *= value;
            z () *= value;
            return  *this;
        }

        Vector3 & operator /= (const TYPE & value)
        {
            if (is_integer)
            {
                x () /= value; y () /= value; z () /= value;
            }
            else
            {
                *this *= TYPE(1) / value;
            }

            return *this;
        }

        Vector3 operator + (const Vector3 & other) const
        {
            return { this->x () + other.x (), this->y () + other.y (), this->z () + other.z () };
        }

        Vector3 operator - (const Vector3 & other) const
        {
            return { this->x () - other.x (), this->y () - other.y (), this->z () - other.z () };
        }

        TYPE   operator * (const Vector3 & other) const
        {
            return this->dot (other);
        }

        Vector3 operator * (const TYPE value) const
        {
            return { x () * value, y () * value, z () * value };
        }

        Vector3 operator / (const TYPE value) const
        {
            return Vector3(*this) /= value;
        }

        Vector3 operator + () const
        {
            return *this;
        }

        Vector3 operator - () const
        {
            return { -x (), -y (), -z () };
        }

    public:

        bool operator == (const Vector3 & other) const
        {
            return this->x () == other.x () && this->y () == other.y () && this->z () == other.z ();
        }

        bool operator != (const Vector3 & other) const
        {
            return not (*this == other);
        }

        bool operator <  (const Vector3 & other) const { return this->squared_modulus () <  other.squared_modulus (); }
        bool operator <= (const Vector3 & other) const { return this->squared_modulus () <= other.squared_modulus (); }
        bool operator >  (const Vector3 & other) const { return this->squared_modulus () >  other.squared_modulus (); }
        bool operator >= (const Vector3 & other) const { return this->squared_modulus () >= other.squared_modulus (); }

    };

    // Definiciones abreviadas de tipos de uso habitual:

    typedef Vector3< int    > Vector3i;
    typedef Vector3< float  > Vector3f;
    typedef Vector3< double > Vector3d;

    // Funciones auxiliares externas a la clase:

    template< typename TYPE >
    inline TYPE modulus (const Vector3< TYPE > & vector)
    {
        return vector.modulus ();
    }

    template< typename TYPE >
    inline TYPE modulus_of (const Vector3< TYPE > & vector)
    {
        return vector.modulus ();
    }

    template< typename TYPE >
    inline TYPE angle_between (const Vector3< TYPE > & a, const Vector3< TYPE > & b)
    {
        static_assert(Vector3< TYPE >::is_real, "math::angle_between<T>() requires a floating point base TYPE.");
        return acos ((a * b) / (a.modulus () * b.modulus ()));
    }

}

// Sobrecarga de algunos operadores fuera de la clase y del namespace, en el ámbito global:

template< typename TYPE >
inline math::Vector3< TYPE > operator * (const TYPE value, const math::Vector3< TYPE > & vector)
{
    return vector * value;
}

template< typename TYPE >
inline std::ostream & operator << (std::ostream & os, const math::Vector3< TYPE > & vector)
{
    return os << '{' << vector.x () << ',' << vector.y () << ',' << vector.z () << '}';
}
