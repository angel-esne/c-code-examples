
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.10

#include <iostream>
#include "String.hpp"

using namespace std;
using namespace utils;

String fun ()
{
    String x("Hello");
    return x;
}

int main ()
{
    String s{"Test"};
    String x{s};
    String y{'a', 'b', 'c', 'd'};
    String z(fun ());
    auto zp = &z;

    z = z + " World" + '!';

    cout << (s == x) << endl;

    if (z)
    {
        for (auto c : z) cout << c;
    }

    return 0;
}
