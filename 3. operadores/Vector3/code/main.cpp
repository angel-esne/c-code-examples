
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.10

#include <iostream>
#include <type_traits>
#include "Vector3.hpp"

using namespace std;
using namespace math;

int main ()
{
    static_assert(is_pod< Vector3 >::value, "PROBLEM: Vector3 is not POD.");

    Vector3 x{  1.f,  0.f,  0.f };
    Vector3 y{  0.f,  1.f,  0.f };
    Vector3 z{  0.f,  0.f,  1.f };
    Vector3 a{ -1.f,  0.f,  0.f };
    Vector3 b{  0.f, -1.f,  0.f };
    Vector3 c{  0.f,  0.f, -1.f };

    cout << "The cross product of " << x << " and " << y << " is " << x.cross (y) << ".\n";
    cout << "The cross product of " << x << " and " << z << " is " << x.cross (z) << ".\n";
    cout << "The cross product of " << y << " and " << z << " is " << y.cross (z) << ".\n";

    cout << "The angle between " << x << " and " << y << " is " << angle_between (x, y) << " radians.\n";
    cout << "The angle between " << x << " and " << a << " is " << angle_between (x, a) << " radians.\n";

    return 0;
}
