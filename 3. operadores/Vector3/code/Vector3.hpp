
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.10

#pragma once

#include <cmath>
#include <ostream>

namespace math
{

    class Vector3
    {

        float coordinates[3];

    public:

        Vector3() = default;
       ~Vector3() = default;
        Vector3(const Vector3 & ) = default;

        Vector3(float x, float y, float z)
        {
            coordinates[0] = x;
            coordinates[1] = y;
            coordinates[2] = z;
        }

        Vector3(float (& array)[3])
        {
            coordinates[0] = array[0];
            coordinates[1] = array[1];
            coordinates[2] = array[2];
        }

    public:

        float & x ()       { return coordinates[0]; }
        float   x () const { return coordinates[0]; }
        float & y ()       { return coordinates[1]; }
        float   y () const { return coordinates[1]; }
        float & z ()       { return coordinates[2]; }
        float   z () const { return coordinates[2]; }

    public:

        float squared_modulus () const
        {
            return x () * x () + y () * y () + z () * z ();
        }

        float modulus () const
        {
            return sqrt (squared_modulus ());
        }

        float length () const
        {
            return modulus ();
        }

        Vector3 & normalize ()
        {
            return *this /= modulus ();
        }

        Vector3 normalized () const
        {
            return *this /  modulus ();
        }

        float dot (const Vector3 & other) const
        {
            return this->x () * other.x () + this->y () * other.y () + this->z () * other.z ();
        }

        Vector3 cross (const Vector3 & other) const
        {
            return
            {
                this->y () * other.z () - other.y () * this->z (),
                this->z () * other.x () - other.z () * this->x (),
                this->x () * other.y () - other.x () * this->y ()
            };
        }

    public:

        Vector3 & operator  = (const Vector3 & ) = default;

        Vector3 & operator += (const Vector3 & other)
        {
            this->x () += other.x ();
            this->y () += other.y ();
            this->z () += other.z ();
            return *this;
        }

        Vector3 & operator -= (const Vector3 & other)
        {
            this->x () -= other.x ();
            this->y () -= other.y ();
            this->z () -= other.z ();
            return *this;
        }

        Vector3 & operator *= (const float & value)
        {
            x () *= value;
            y () *= value;
            z () *= value;
            return  *this;
        }

        Vector3 & operator /= (const float & value)
        {
            return *this *= 1.f / value;
        }

        Vector3 operator + (const Vector3 & other) const
        {
            return { this->x () + other.x (), this->y () + other.y (), this->z () + other.z () };
        }

        Vector3 operator - (const Vector3 & other) const
        {
            return { this->x () - other.x (), this->y () - other.y (), this->z () - other.z () };
        }

        float   operator * (const Vector3 & other) const
        {
            return this->dot (other);
        }

        Vector3 operator * (const float value) const
        {
            return { x () * value, y () * value, z () * value };
        }

        Vector3 operator / (const float value) const
        {
            return Vector3(*this) /= value;
        }

        Vector3 operator + () const
        {
            return *this;
        }

        Vector3 operator - () const
        {
            return { -x (), -y (), -z () };
        }

    public:

        bool operator == (const Vector3 & other) const
        {
            return this->x () == other.x () && this->y () == other.y () && this->z () == other.z ();
        }

        bool operator != (const Vector3 & other) const
        {
            return not (*this == other);
        }

        bool operator <  (const Vector3 & other) const { return this->squared_modulus () <  other.squared_modulus (); }
        bool operator <= (const Vector3 & other) const { return this->squared_modulus () <= other.squared_modulus (); }
        bool operator >  (const Vector3 & other) const { return this->squared_modulus () >  other.squared_modulus (); }
        bool operator >= (const Vector3 & other) const { return this->squared_modulus () >= other.squared_modulus (); }

    };

    // Funciones auxiliares externas a la clase:

    inline float modulus (const Vector3 & vector)
    {
        return vector.modulus ();
    }

    inline float modulus_of (const Vector3 & vector)
    {
        return vector.modulus ();
    }

    inline float angle_between (const Vector3 & a, const Vector3 & b)
    {
        return acos ((a * b) / (a.modulus () * b.modulus ()));
    }

}

// Sobrecarga de algunos operadores fuera de la clase y del namespace, en el ámbito global:

inline math::Vector3 operator * (const float value, const math::Vector3 & vector)
{
    return vector * value;
}

inline std::ostream & operator << (std::ostream & os, const math::Vector3 & vector)
{
    return os << '{' << vector.x () << ',' << vector.y () << ',' << vector.z () << '}';
}
